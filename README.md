# GlobPatternMatcher

**File structure**
1. Common_src : The source files are here
2. windows : project specific files are here (Microsoft Visual Studio Community 2019 Version 16.5.4)
3. linux : makefile for linux build (tested with g++ (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0)

**How to use the software**
1. Windows
When you build the main.cpp (in the common_src folder) with the visual studio project files you will get a windows.exe file.
The exe requires 2 argument (first argument is the base path, second argument is the pattern).

for example: windows.exe d:\aimotive\input\ parentfolder\**\subfolder*\*.*

2. linux
When you build main.cpp (in the common_src folder) with the makefile (linux folder) you will get a linux.exe file.
The exe requires 2 argument (first argument is the base path, second argument is the pattern).

for example: ./linux.exe /home/akos/aimotive parentfolder/**/subfolder*/*.*

**output format**
The exe will printout the absolute path with a new line to the standard output.
