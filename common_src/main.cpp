
#include <iostream>
#include <exception>

#include <vector>
#if defined(WIN32) || defined(_WIN32) 
    #include <windows.h>
#else 
    #include <sys/types.h>
    #include <dirent.h>
#endif

using namespace std;

#if defined(WIN32) || defined(_WIN32) 
    #define PATH_SEPARATOR "\\" 
#else 
    #define PATH_SEPARATOR "/" 
#endif 

struct InvalidInputException : public exception {
    const char* what() const throw () {
        return "Invalid input format";
    }
};

/* check last char is separator character or not */
bool checkLastChar(string s) {
    if (0 == string(1, s.back()).compare(PATH_SEPARATOR)) {
        return true;
    }
    return false;
}

/* Split the query path to a vector<string> */
vector<string> split(const std::string& path, std::string &delimiter) {
    vector<string> splited_str;
    std::string s = path;
    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        splited_str.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    splited_str.push_back(s);
    return splited_str;
}

/* Convert wstring to string */
string WcharToString(wstring ws) {
    string str(ws.begin(), ws.end());
    return str;
}

/* check the query is matching with the file/directory name */
bool useThisFile(const string name, const string query) {
    string newQuery = query;
    string newName = name;
    size_t name_found;
    string findThisInName;
    /* check that the qurey contains star */
    size_t query_found = newQuery.find("*");
    if (query_found == string::npos) {
        /* exact match required */
        if (name != query) {
            return false;
        }
    }
    /* create substrings between stars */
    while (query_found != string::npos) {
        /* create substring */
        findThisInName = newQuery.substr(0, query_found);
        /* substring is not star */
        if (findThisInName.size() != 0) {
            /* check that the substring is in */
            name_found = newName.find(findThisInName);
            if (name_found != string::npos) {
                /* create new substring from the remaining chars */
                newName = newName.substr(name_found + findThisInName.size());
            }
            else {
                return false;
            }
        }
        /* create new substring from the remaining chars */
        newQuery = newQuery.substr(query_found + 1);
        query_found = newQuery.find("*");
    }
    /* create substring */
    findThisInName = newQuery.substr(0, query_found);
    /* substring is not star */
    if (findThisInName.size() != 0) {
        name_found = newName.find(findThisInName);
        if (name_found != string::npos) {
            /* find last occurence */
            while (name_found != string::npos) {
                /* create new substring from the remaining chars */
                newName = newName.substr(name_found + findThisInName.size());
                name_found = newName.find(findThisInName);
            }
        }
        else {
            return false;
        }
        /* last char is not star so we need exact match */
        if (newName.size() != 0)
            return false;
    }
    return true;
}

/* Print string to stdout */
void saveResult(string path) {
    cout << path << endl;
}

/* return the asked directories and show the files */
#if defined(WIN32) || defined(_WIN32) 
vector<string> lowLevelReadDir(const string & basePath, const string query, bool lastPath) {
    /* matched directory paths saved to this variable */
    vector<string> directories;

    /* create the asked path */
    string qureryPath = basePath;
    qureryPath.append(PATH_SEPARATOR);
    qureryPath.append(query);

    /* convert it to LPCWSTR */
    wstring stemp = wstring(qureryPath.begin(), qureryPath.end());
    LPCWSTR sw = stemp.c_str();
    WIN32_FIND_DATA data;
    HANDLE hFind;

    /* checks that the directory is empty or the directory/file is not exists */
    if ((hFind = FindFirstFile(sw, &data)) != INVALID_HANDLE_VALUE) {
        do {
            /* don't use . and .. folders */
            if (0 != WcharToString(data.cFileName).compare(".") && 0 != WcharToString(data.cFileName).compare("..")) {
                /* check that the file/directory name matches the query */
                if (useThisFile(WcharToString(data.cFileName), query)) {
                    /* create the matched directory/file absolute path */
                    string path = basePath;
                    if(!checkLastChar(basePath))
                        path.append(PATH_SEPARATOR);
                    path.append(WcharToString(data.cFileName));
                    /* if it is directory save it for later use */
                    if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
                    {
                        /* if last pattern is a directory pattern show invalid input*/
                        if (lastPath && 0 != query.compare("*")) throw InvalidInputException();
                        directories.push_back(path);
                        continue;
                    }
                    /* at last query show the result */
                    if (lastPath) {
                        saveResult(path);
                    }
                }
            }  
        } while (FindNextFile(hFind, &data) != 0);
        FindClose(hFind);
    }
    return directories;
}
#else
/* return the asked directories and show the files */
vector<string> lowLevelReadDir(const std::string& basePath, const string query, bool lastPath)
{
    /* matched directory paths saved to this variable */
    vector<string> directories;
    DIR* dirp = opendir(basePath.c_str());
    struct dirent * dp;

    /* checks that the directory is empty or the directory/file is not exists */
    while ((dp = readdir(dirp)) != NULL) {
        /* don't use . and .. folders */
        if (0 != string(dp->d_name).compare(".") && 0 != string(dp->d_name).compare("..")) {
            /* check that the file/directory name matches the query */
            if (useThisFile(string(dp->d_name), query)) {
                string path = basePath;
                if (!checkLastChar(basePath))
                    path.append(PATH_SEPARATOR);
                path.append(string(dp->d_name));
                /* if it is directory save it for later use */
                if (dp->d_type == DT_DIR) {
                    /* if last pattern is a directory pattern show invalid input*/
                    if (lastPath && 0 != query.compare("*")) throw InvalidInputException();
                        directories.push_back(path);
                        continue;
                }
                /* at last query show the result */
                if(lastPath){
                    saveResult(path);
                }
            }
        }
    }
    closedir(dirp);
    return directories;
}
#endif

/* recursive function for glob pattern matcher */
void readDir(const vector<string> basePaths, vector<string> queries) {
    /* the query is the last one so we need to show the output */
    bool lastPath = false;
    vector<string> _queries;
    /* iterate over directories */
    for (auto basePath : basePaths) {
        /* save the parameter queries */
        _queries = queries;
        vector<string>directories;
        /* last query so we need to show the output */
        if (_queries.size() == 1) {
            directories = lowLevelReadDir(basePath, _queries[0], true);
        }
        else {
            /* ask directories from base path and the next query */
            directories = lowLevelReadDir(basePath, _queries[0], false);
            /* we have directories */
            if(directories.size() != 0) {
                /* the query is "**" */
                if (0 == _queries[0].compare("**")){
                    /* call readDir recursively */
                    readDir(directories, queries);
                }
                /* delete the used query */
                _queries.erase(_queries.begin(), _queries.begin() + 1);
                /* call readDir recursively */
                readDir(directories, _queries);
            }
        }
    }
    return;
    
}

/* last pattern must be valid file pattern */
void checkLastPattern(string query) {
    if (checkLastChar(query))
        throw InvalidInputException();
    return;
}

int main(int argc, char** argv)
{
    /* first argument is base path*/
    string basePath = argv[1];
    vector<string> basePaths;
    basePaths.push_back(basePath);
    /* second argument is the pattern*/
    string query = argv[2];
    checkLastPattern(query);
    string delimiter = PATH_SEPARATOR;
    vector<string> _query = split(query, delimiter);
    readDir(basePaths, _query);
}

